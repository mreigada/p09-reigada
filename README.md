Matt Reigada


Rock, Paper, Capture iOS game.  Game ends when only one player can move pieces, leaving them the winner.
Paper captures rock,
rock captures scissor,
scissors captures paper.
Enjoy.