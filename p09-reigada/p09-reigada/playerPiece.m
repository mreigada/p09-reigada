
#import <Foundation/Foundation.h>
#import "playerPiece.h"
#import "board.h"
#import "player.h"

@implementation playerPiece

	-(id)init:				(SKScene*)		s
		onBoard:			(board*)		b
		withPlayerType:		(locationState) playerType
		withTypeOfPiece:	(pieceKind)		RPS
		withX:				(int) 			x
		withY:				(int)			y{
		
		_playerState = playerType;
		_rps = RPS;
		_xGridPos = x;
		_yGridPos = y;
		_brd = b;
		_scene = s;
		
		[self updateSprite];
		
		return self;
	}

	//method will attempt to move player and will return false if invalid move attempted
	-(playerPiece*)movePlayer: (direction) d withPlayers:(NSMutableArray*)players{
		playerPiece* rv = self;
		
		int xoffset = 0;
		int yoffset = 0;
		
		//get yoffset from state
		if(d == NW || d == N || d == NE){
			yoffset = 1;
		}else if (d == SW || d == S || d == SE){
			yoffset = -1;
		}
		
		//get xoffset from state
		if(d == NW || d == W || d == SW){
			xoffset = -1;
		}else if(d == NE || d == E || d == SE){
			xoffset = 1;
		}
		
		//cannot move to self, thus check that direction has been set validly
		if(yoffset == 0 && xoffset == 0){
			return NULL;
		}
		
		//get grid/array coordinates of both nodes in requested move
		int sx = _xGridPos;
		int sy = _yGridPos;
		int ex = _xGridPos + xoffset;
		int ey = _yGridPos + yoffset;
		int eex = ex + xoffset;	//these are locations 2 away
		int eey = ey + yoffset;	//used to test piece approaching
		
		NSMutableArray* firstRow = [_brd locations][0];
		int height = (int)([_brd locations].count);
		int width = (int)firstRow.count;
		
		//cannot move off grid, return false
		if(ex < 0 || ex >= width || ey < 0 || ey >= height){
			return NULL;
		}
		
		//get both board locations
		boardLocation* currentNode = [_brd locations][sy][sx];
		boardLocation* nextNode = [_brd locations][ey][ex];
		
		//below block ensure pieces may not move into locations held by other pieces
		{
			//node is a node at the pt to move to
			//if node is a game piece
			for(player* player in players){
				//if any player has  piece there then don't move
				if([player getPieceAt:ex withY:ey] != NULL){
					return NULL;
				}
			}
		}
		
		{
			for(player* player in players){
				playerPiece* takenPiece = [player getPieceAt:eex withY:eey];
				if(takenPiece != NULL){
					if(((takenPiece.rps + 1) % 3) == self.rps){
						rv = takenPiece;
					}
				}
			}
		}
		
		//get both list of edges leaving requested nodes, used for edge coloring
		NSMutableArray* currentNodeEdges = [_brd edges][sy][sx];
		NSMutableArray* nextNodeEdges = [_brd edges][ey][ex];
		
		//first flag detects edge coloring one way, next flag detects edge coloring the other way
		bool moveFoundA = false;
		bool moveFoundB = false;
		
		//iterate edges leaving first node
		for(int i = 0; i < currentNodeEdges.count; i++){
			//get each edge
			edge* e = currentNodeEdges[i];
			//if edge leads to requested node and is not yet used then color it
			if(e.end == nextNode && [e locState] == NOT_USED){
				//[e updateEdgeState:_playerState];
				moveFoundA = true;
			}
		}
		
		//iterate edges leaving second node
		for(int i = 0; i < nextNodeEdges.count; i++){
			//get each edge
			edge* e = nextNodeEdges[i];
			//if edge leads to requested end node and state of edge is unused then color it
			if(e.end == currentNode && [e locState] == NOT_USED){
				//[e updateEdgeState:_playerState];
				moveFoundB = true;
			}
		}
		
		//assert that edge has been colored both ways
		assert(moveFoundA == moveFoundB);
		
		if(moveFoundA && moveFoundB){
			//set new grid coordinates of player
			_xGridPos = ex;
			_yGridPos = ey;
			//move player to end of edge taken
			CGPoint end = [[nextNode sprite] position];
			[_sprite runAction:[SKAction moveTo:end duration:[board MOVEMENT_DURATION]]];
			//return true for successful move
			return rv;
		}

		//return false for unsuccessful move
		return false;
	}

	//method shall state if player has remaining moves, if false then game over
	-(bool)playerHasMoves:(NSMutableArray*)players{

		for(int xoffset = -1; xoffset <= 1; xoffset++){
			for(int yoffset = -1; yoffset <= 1; yoffset++){
				if(xoffset != 0 || yoffset != 0){
					//above conditions determine valid neighborhood location
					
					int ex = _xGridPos + xoffset;
					int ey = _yGridPos + yoffset;
					
					NSMutableArray* row = [_brd locations][0];
					int w = (int)row.count;
					int h = (int)[_brd locations].count;
					
					//if valid grid position
					if(ex >= 0 && ex >= 0 && ex < w && ey < h){
						bool freeSpot = true;
						
						//if spot has no player on it
						for(player* p in players){
							if([p getPieceAt:ex withY:ey] != NULL){
								freeSpot = false;
							}
						}
						//if nobody else has a piece on this valid neighboring location, then this piece has a move
						if(freeSpot){
							return true;
						}
					}
				}
			}
		}

		return false;
	}

	-(void)updatePlayer:(locationState) owner{
		_playerState = owner;
		[self updateSprite];
	}

	-(CGPathRef) triangleInRect:(CGRect)rect{
		CGFloat offsetX = CGRectGetMidX(rect);
		CGFloat offsetY = CGRectGetMidY(rect);
		UIBezierPath* bezierPath = [UIBezierPath bezierPath];
		
		[bezierPath moveToPoint: CGPointMake(offsetX, 0)];
		[bezierPath addLineToPoint: CGPointMake(-offsetX, offsetY)];
		[bezierPath addLineToPoint: CGPointMake(-offsetX, -offsetY)];
		[bezierPath closePath];
		return bezierPath.CGPath;
	}

	-(void)updateSprite{
		if(_scene != NULL && _brd != NULL && _sprite != NULL){
			[_sprite removeFromParent];
		}
	
		SKColor* fillColor;
		SKColor* borderColor;
		switch(_playerState){
			case NOT_USED:
				fillColor = [SKColor orangeColor];
				borderColor = [SKColor yellowColor];
			break;
			case P1:
				fillColor = PLAYER_COLOR;
				borderColor = [SKColor blackColor];
			break;
			case P2:
				fillColor = AI_COLOR;
				borderColor = [SKColor blackColor];
			break;
			case P3:
				fillColor = P3_COLOR;
				borderColor = [SKColor blackColor];
			break;
			case P4:
				fillColor = P4_COLOR;
				borderColor = [SKColor blackColor];
			break;
			default:
				fillColor = [SKColor purpleColor];
				borderColor = [SKColor cyanColor];
			break;
		}
	
		CGFloat tmp_size = 1.8 * [board SELECTED_LOCATION_SIZE];
		CGFloat rockScale = 1.2;
		CGFloat paperScale = 2.0;
		CGFloat scissorsScale = 2.75;
		
		CGRect rect = CGRectMake(0, 0, tmp_size * scissorsScale, tmp_size * scissorsScale);
				
		SKShapeNode *shape = [SKShapeNode node];
		shape.path = [self triangleInRect:rect];
		
		[shape runAction:[SKAction rotateByAngle:(M_PI / 2) duration:0]];
		
		switch(_rps){
			case ROCK:
				_sprite = [SKShapeNode shapeNodeWithCircleOfRadius:rockScale*tmp_size];
			break;
			case PAPER:
				_sprite = [SKShapeNode shapeNodeWithRectOfSize:CGSizeMake(paperScale*tmp_size, paperScale*tmp_size)];
			break;
			case SCISSORS:
				_sprite = shape;
			break;
		}
		
		[_sprite setStrokeColor:borderColor];
		[_sprite setFillColor:fillColor];
		
		[_sprite setName:GAME_PIECE_NAME];
		
		NSMutableArray* locs = [_brd locations];
		NSMutableArray* row = locs[_yGridPos];
		boardLocation* playerLoc = row[_xGridPos];
		CGPoint pos = [playerLoc sprite].position;
		[_sprite setPosition: pos];
		[_sprite setZPosition:4.0];
		
		[_scene addChild:_sprite];
	}

	-(void)highlightPiece{
		[_sprite setStrokeColor:PLAYERCOLOR(_playerState)];
		[_sprite setGlowWidth:SELECTED_GLOW_WIDTH];
	}

	-(void)dehighlightPiece{
	[_sprite setStrokeColor:[SKColor blackColor]];
		[_sprite setGlowWidth:0.0];
	}

@end