//
//  menuScene.h
//  p09-reigada
//
//  Created by Matthew Reigada on 5/17/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#ifndef menuScene_h
#define menuScene_h

#import <SpriteKit/SpriteKit.h>

@interface menuScene : SKScene

	@property int num_players;

	@property SKLabelNode* players;

@end

#endif /* menuScene_h */
