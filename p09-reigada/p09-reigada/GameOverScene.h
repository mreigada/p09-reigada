//
//  GameOverScene.h
//  p09-reigada
//
//  Created by Matthew Reigada on 5/15/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#ifndef GameOverScene_h
#define GameOverScene_h

#import <SpriteKit/SpriteKit.h>
#import "board.h"
#import "playerPiece.h"
#import "player.h"

@interface GameOverScene : SKScene

	@property int winner;

@end

#endif /* GameOverScene_h */
