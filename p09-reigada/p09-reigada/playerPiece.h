
#ifndef playerPiece_h
#define playerPiece_h

#import <SpriteKit/SpriteKit.h>
#import "board.h"
#import "boardLocation.h"
#import "edge.h"

@interface playerPiece: NSObject

	@property int xGridPos;
	@property int yGridPos;
	@property SKShapeNode* sprite;
	@property board* brd;
	@property locationState playerState;
	@property pieceKind rps;
	@property SKScene* scene;

	//constructor method with scene to add sprite to
	-(id)init:				(SKScene*)		s
		onBoard:			(board*)		b
		withPlayerType:		(locationState) playerType
		withTypeOfPiece:	(pieceKind)		RPS
		withX:				(int) 			x	//note: x,y are grid coordinates within board
		withY:				(int)			y;

	//method will return NULL if failed move attempt,
	//return self if successful move with no taken piece, otherwise returns taken piece
	-(playerPiece*)movePlayer: (direction) d withPlayers:(NSMutableArray*)players;

	//method shall state if player has remaining moves, if false then game over
	-(bool)playerHasMoves:(NSMutableArray*)players;

	//overwrites the owner of this piece and redraws
	-(void)updatePlayer:(locationState) owner;

	-(void)updateSprite;

	-(void)highlightPiece;

	-(void)dehighlightPiece;

@end


#endif /* player_h */
