//
//  GameScene.h
//  p09-reigada
//

//  Copyright (c) 2016 Matthew Reigada. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "board.h"
#import "playerPiece.h"
#import "player.h"

@interface GameScene : SKScene

	@property board* brd;
	@property int num_players;	//[1, 4]
	@property int num_ai;		//[0, 3]
	@property bool updateReady;
	@property NSMutableArray* players;
	@property NSMutableArray* playerTokens;
	@property int currentPlayerMove;
	@property SKShapeNode* playerUp;
	@property SKLabelNode* playerUpLabel;

@end
