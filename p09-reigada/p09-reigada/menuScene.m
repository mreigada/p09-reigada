//
//  menuScene.m
//  p09-reigada
//
//  Created by Matthew Reigada on 5/17/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "menuScene.h"

#import "globalConstants.h"

#import "GameScene.h"

@implementation menuScene : SKScene

	-(void)didMoveToView:(SKView *)view {
		[self setBackgroundColor:[SKColor blackColor]];
	
	    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:PREFERRED_FONT];
    	label.fontSize = MENU_FONT_SIZE;
		label.fontColor = [SKColor whiteColor];
    	label.text = [NSString stringWithFormat:@"Rock, Paper, Capture."];
		label.position = CGPointMake(self.size.width / 2, 2.0 * self.size.height / 3.0);
		[self addChild:label];
		
		label = [SKLabelNode labelNodeWithFontNamed:PREFERRED_FONT];
    	label.fontSize = MENU_FONT_SIZE;
		label.fontColor = [SKColor greenColor];
    	label.text = [NSString stringWithFormat:START_TEXT];
		label.name = START_TEXT;
		label.position = CGPointMake(self.size.width / 2, self.size.height / 4.0);
		_players = label;
		[self addChild:label];
		
		_num_players = 2;
		
		label = [SKLabelNode labelNodeWithFontNamed:PREFERRED_FONT];
    	label.fontSize = NUM_PLAYERS_FONT_SIZE;
		label.fontColor = [SKColor blueColor];
    	label.text = [NSString stringWithFormat:@"%d", _num_players];
		label.position = CGPointMake(self.size.width / 2, self.size.height / 2.3);
		_players = label;
		[self addChild:label];
	}

	-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
		for(UITouch* touch in touches){
			NSArray *nodes = [self nodesAtPoint: [touch locationInNode:self]];
			for(SKNode* node in nodes){
				if(node == _players){
					_num_players++;
					if(_num_players > 4){
						_num_players = 2;
					}
					[_players removeFromParent];
					_players = [SKLabelNode labelNodeWithFontNamed:PREFERRED_FONT];
    				_players.fontSize = NUM_PLAYERS_FONT_SIZE;
					_players.fontColor = [SKColor blueColor];
    				_players.text = [NSString stringWithFormat:@"%d", _num_players];
					_players.position = CGPointMake(self.size.width / 2, self.size.height / 2.3);
					[self addChild:_players];
				}else if([node.name isEqualToString: START_TEXT]){
					//transition for victory screen here
					GameScene* s = [[GameScene alloc] initWithSize:self.size];
					s.num_players = _num_players;
					s.num_ai = 0;
		
					SKTransition *transition = [SKTransition flipVerticalWithDuration:1.0];
        			[self.view presentScene:s transition:transition];
				}
			}
		}
	}

@end