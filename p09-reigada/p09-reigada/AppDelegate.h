//
//  AppDelegate.h
//  p09-reigada
//
//  Created by Matthew Reigada on 5/3/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

