//
//  player.h
//  p09-reigada
//
//  Created by Matthew Reigada on 5/13/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#ifndef player_h
#define player_h

#import <SpriteKit/SpriteKit.h>
#import "playerPiece.h"
#import "globalConstants.h"

@interface player : NSObject

	@property NSMutableSet* pieceCont;
	@property playerPiece* selectedPiece;
	@property locationState name;
	@property bool isAI;

	//empty constructor
	-(id) init: (locationState) playerName IsAI:(bool) ai;

	-(void) givePieceTo:(player*) opponent pieceGiven:(playerPiece*) piece;

	//will add piece to player
	-(void) addPiece:(playerPiece*) p;

	//will remove piece from player, will return false if not found
	-(bool) removePiece:(playerPiece*) p;

	//every player receives every node touched, if that node matches one of this player's pieces
	//and this player is not an AI then this player will set that as their selected piece
	-(bool) selectPiece:(SKNode*) sprite;

	//will attempt move, returns false if invalid move received
	-(bool) performMove:(direction) moveDirection withPieces:(NSMutableArray*)players;

	//will return piece at location in board, NULL if not found
	-(playerPiece*) getPieceAt:(int) x withY:(int) y;

	-(bool) hasMovablePieces:(NSMutableArray*)players;

	-(void) select;

	-(void) unselect;

@end

#endif /* player_h */
