//
//  player.m
//  p09-reigada
//
//  Created by Matthew Reigada on 5/13/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "player.h"

@implementation player : NSObject

	-(id) init: (locationState) playerName IsAI:(bool) ai{
		self = [super init];
		_pieceCont = [[NSMutableSet alloc] init];
		_name = playerName;
		_isAI = ai;
		return self;
	}

	-(void) givePieceTo:(player*) opponent pieceGiven:(playerPiece*) piece{
		assert([self removePiece:piece] == true);
		[opponent addPiece:piece];
	}

	//will add piece to player and redraw with new color
	-(void) addPiece:(playerPiece*) p{
		[_pieceCont addObject:p];
		[p updatePlayer:_name];
	}

	//will remove piece from player, will return false if not found
	-(bool) removePiece:(playerPiece*) p{
		if([_pieceCont containsObject:p]){
			[_pieceCont removeObject:p];
			if(p == _selectedPiece){
				[_selectedPiece dehighlightPiece];
				_selectedPiece = NULL;
			}
			return true;
		}
		return false;
	}

	//return true and select piece
	-(bool) selectPiece:(SKNode*) sprite{
		for(playerPiece* piece in _pieceCont){
			if([piece.sprite isEqual:sprite]){
				if(_selectedPiece != nil){
					[_selectedPiece dehighlightPiece];
				}
				_selectedPiece = piece;
				[_selectedPiece highlightPiece];
				return true;
			}
		}
		return false;
	}

	-(bool) performMove:(direction) moveDirection withPieces:(NSMutableArray*)players{
		if(_selectedPiece == NULL){
			return false;
		}else{
			playerPiece* p = [_selectedPiece movePlayer:moveDirection withPlayers:players];
			
			if(p == NULL){
				return false;
			}
			
			//this will happen if p is captured
			if(p != _selectedPiece){
				player* otherPlayer = players[p.playerState - 1];
				
				//request piece from other player since it has been captured
				[otherPlayer givePieceTo:self pieceGiven:p];
			}
			
			return true;
		}
	}

	-(playerPiece*) getPieceAt:(int) x withY:(int) y{
		for(playerPiece* p in _pieceCont){
			if(p.xGridPos == x && p.yGridPos == y){
				return p;
			}
		}
		return NULL;
	}

	-(bool) hasMovablePieces:(NSMutableArray*)players{
		for(playerPiece* piece in _pieceCont){
			if([piece playerHasMoves:players]){
				return true;
			}
		}
		return false;
	}

	-(void) select{
		if(_selectedPiece){
			[_selectedPiece highlightPiece];
		}
	}

	-(void) unselect{
		if(_selectedPiece){
			[_selectedPiece dehighlightPiece];
		}
	}

@end