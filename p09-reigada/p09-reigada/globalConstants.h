//
//  globalConstants.h
//  p09-reigada
//
//  Created by Matthew Reigada on 5/3/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#ifndef globalConstants_h
#define globalConstants_h

#define PREFERRED_FONT @"HelveticaNeue-CondensedBlack"//@"Optima-ExtraBlack"

#define GRASS_COLOR [SKColor colorWithRed:0.2 green:0.5 blue:0.1 alpha:0.4]
#define ROAD_COLOR [SKColor colorWithRed:0.9 green:0.8 blue:0.6 alpha:0.8]

#define TOKEN_DARKEN 0.2
#define TRANSPARENCY 0.7

#define AI_R 0.0
#define AI_G 1.0
#define AI_B 0.0

#define PLAYER_R 1.0
#define PLAYER_G 0.0
#define PLAYER_B 0.0

#define P3_R 1.0
#define P3_G 1.0
#define P3_B 0.0

#define P4_R 0.0
#define P4_G 0.0
#define P4_B 1.0

#define PLAYER_COLOR		[SKColor colorWithRed:PLAYER_R-TOKEN_DARKEN green:PLAYER_G-TOKEN_DARKEN blue:PLAYER_B-TOKEN_DARKEN alpha:1.0]
#define AI_COLOR 			[SKColor colorWithRed:AI_R-TOKEN_DARKEN green:AI_G-TOKEN_DARKEN blue:AI_B-TOKEN_DARKEN alpha:1.0]
#define P3_COLOR			[SKColor colorWithRed:P3_R-TOKEN_DARKEN green:P3_G-TOKEN_DARKEN blue:P3_B-TOKEN_DARKEN alpha:1.0]
#define P4_COLOR			[SKColor colorWithRed:P4_R-TOKEN_DARKEN green:P4_G-TOKEN_DARKEN blue:P4_B-TOKEN_DARKEN alpha:1.0]

#define BOARD_SIZE ((2 * 4) + 1 )
#define BMID ((BOARD_SIZE + 0) / 2)

#define PLAYER_EDGE_COLOR	[SKColor colorWithRed:PLAYER_R green:PLAYER_G blue:PLAYER_B alpha:TRANSPARENCY]
#define AI_EDGE_COLOR 		[SKColor colorWithRed:AI_R green:AI_G blue:AI_B alpha:TRANSPARENCY]
#define P3_EDGE_COLOR		[SKColor colorWithRed:P3_R green:P3_G blue:P3_B alpha:TRANSPARENCY]
#define P4_EDGE_COLOR		[SKColor colorWithRed:P4_R green:P4_G blue:P4_B alpha:TRANSPARENCY]
#define DEFAULT_EDGE_COLOR	[SKColor colorWithRed:0.7 green:0.7 blue:0.7 alpha:0.4]

#define DOWN_PT_COLOR		[SKColor colorWithRed:0.7 green:0.0 blue:0.7 alpha:0.9]
#define UP_PT_COLOR			[SKColor colorWithRed:1.0 green:0.5 blue:0.0 alpha:0.9]
#define UP_DOWN_SCALAR		3
#define CONNECTION_SCALAR	((UP_DOWN_SCALAR *  2 ) / 3)

#define SELECTED_GLOW_WIDTH 4

#define START_TEXT @"Start?"

#define NO_TAKEN_PIECE 0x01

#define GAME_PIECE_NAME @"gp"

#define MENU_FONT_SIZE 36
#define NUM_PLAYERS_FONT_SIZE 50

typedef enum locationStateType{
	NOT_USED    = 0,
	P1			= 1,
	P2			= 2,
	P3			= 3,
	P4			= 4
} locationState;

#define PLAYERCOLOR(p) 													\
			(p == P1) ? (PLAYER_COLOR) :								\
				(p == P2) ? (AI_COLOR) :								\
					(p == P3) ? (P3_COLOR) :							\
						(p == P4) ? (P4_COLOR) : ([SKColor whiteColor])

typedef enum pieceKindType{
	ROCK		= 0,
	PAPER		= 1,
	SCISSORS	= 2
} pieceKind;

typedef enum directionType{
	NW	= 0,
	N 	= 1,
	NE	= 2,
	W	= 3,
	E	= 4,
	SW	= 5,
	S 	= 6,
	SE	= 7
} direction;

#endif /* globalConstants_h */
