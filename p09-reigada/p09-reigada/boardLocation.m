//
//  boardLocation.m
//  project8
//
//  Created by Matthew Reigada on 4/14/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "boardLocation.h"

#import "board.h"

@implementation boardLocation : NSObject

	//constructor for object
	-(id)init:(CGPoint) loc{
		_location = loc;
		return self;
	}



	//This procedure sets the location of this point
	-(void)setPoint:(CGPoint) loc {
		_location = loc;
		
	}

	-(SKNode*)createNode:(CGSize) viewDim withParity:(int) p{
		//SKShapeNode* retVal = [SKShapeNode shapeNodeWithCircleOfRadius:[board LOCATION_SIZE]];
		
		double sw = 1.75;
		double sh = 1.2;
		double w = viewDim.width / (7.0 * sw);
		double h = viewDim.height / (7.0 * sw * sh);
		SKShapeNode* retVal = 	[SKShapeNode shapeNodeWithRect:
									CGRectMake( -w / 2.0, -h / 2.0, w, h)
								];
								
		retVal.fillColor = (p % 2)?[SKColor darkGrayColor]:[SKColor blackColor];
		//CGFloat w = min(viewDim.width, viewDim.height);
		//CGFloat h = max(viewDim.width, viewDim.height);
		[retVal setPosition:CGPointMake(_location.x * viewDim.width, _location.y * viewDim.height)];
		[retVal setZPosition:3];
		_sprite = retVal;
		return retVal;
	}

@end