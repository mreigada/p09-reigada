//
//  GameScene.m
//  p09-reigada
//
//  Created by Matthew Reigada on 5/3/16.
//  Copyright (c) 2016 Matthew Reigada. All rights reserved.
//

#import "GameScene.h"
#import "GameOverScene.h"
#import "globalConstants.h"

@implementation GameScene

int startX[3][4] =	{
						{ BMID - 1,	BMID + 1,	0,	BOARD_SIZE - 1 },
						{ BMID,		BMID,		0,	BOARD_SIZE - 1 },
						{ BMID + 1,	BMID - 1,	0,	BOARD_SIZE - 1 }
					};

int startY[3][4] =	{
						{ BOARD_SIZE - 1,	0,	BMID - 1,	BMID + 1 },
						{ BOARD_SIZE - 1,	0,	BMID,		BMID},
						{ BOARD_SIZE - 1,	0,	BMID + 1,	BMID - 1 }
					};

-(void)didMoveToView:(SKView *)view {
	_updateReady = false;
	_currentPlayerMove = 0;
	assert(_num_players < 5 && _num_players > 1);
	assert(_num_ai >= 0 && _num_ai <= _num_players);
	
	_players = [[NSMutableArray alloc] init];
	_playerTokens = [[NSMutableArray alloc] init];
	[self setBackgroundColor:[SKColor blackColor]];
	[board SET_BOARD_SIZE:BOARD_SIZE];
	_brd = [[board alloc] init];
	[_brd addToView:self];
	
	//each player
	for( locationState currP = P1; currP <= _num_players; currP++ ){
		player* p = [[player alloc] init:currP IsAI:((_num_players - currP) <= _num_ai)];
		[_players addObject: p];
		playerPiece* tmp = NULL;
		
		[p addPiece:
			(tmp = [[playerPiece alloc]
						init:self
						onBoard: _brd
						withPlayerType:currP
						withTypeOfPiece:ROCK
						withX:startX[ROCK][currP - 1]
						withY:startY[ROCK][currP - 1]
					]
			)
		];
		[_playerTokens addObject:tmp];
		
		[p addPiece:
			(tmp = [[playerPiece alloc]
					init:self
						onBoard: _brd
						withPlayerType:currP
						withTypeOfPiece:PAPER
						withX:startX[PAPER][currP - 1]
						withY:startY[PAPER][currP - 1]
					]
			)
		];
		[_playerTokens addObject:tmp];
		
		[p addPiece:
			(tmp = [[playerPiece alloc]
					init:self
						onBoard: _brd
						withPlayerType:currP
						withTypeOfPiece:SCISSORS
						withX:startX[SCISSORS][currP - 1]
						withY:startY[SCISSORS][currP - 1]
					]
			)
		];
		[_playerTokens addObject:tmp];
	}
	_updateReady = true;
}

bool touchDown = false;
bool commitTouch = false;

direction touchVector;

CGPoint current;
CGPoint last;

SKShapeNode* s;
SKShapeNode* e;
SKNode* connection;

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    /* Called when a touch begins */
    for (UITouch *touch in touches) {
		if(touchDown == false){
			touchDown = true;
			last = [touch locationInNode:self];
			
			s = [SKShapeNode shapeNodeWithCircleOfRadius:[board LOCATION_SIZE] * UP_DOWN_SCALAR];
			
			[s setPosition:[touch locationInNode:self]];
			s.zPosition = 10;
			
			[s setFillColor:DOWN_PT_COLOR];
			[s setStrokeColor:[SKColor grayColor]];
			
			[self addChild:s];
		}else{

		}
		current = [touch locationInNode:self];
		break;
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
	/* Called when a touch begins */
    for (UITouch *touch in touches) {
		current = [touch locationInNode:self];

		if(e != NULL){
			[e removeFromParent];
			e = NULL;
		}
		
		if(connection != NULL){
			[connection removeFromParent];
			connection = NULL;
		}
		
		current = [touch locationInNode:self];
		e = [SKShapeNode shapeNodeWithCircleOfRadius:[board LOCATION_SIZE] * UP_DOWN_SCALAR];
		[e setPosition:[touch locationInNode:self]];
		e.zPosition = 10;
		[e setFillColor:UP_PT_COLOR];
		[e setStrokeColor:[SKColor grayColor]];
		
		SKShapeNode* retVal = [[SKShapeNode alloc] init];
		
		CGMutablePathRef pathToDraw = CGPathCreateMutable();
		
		//start pos
		double sx = s.position.x;
		double sy =	s.position.y;
		
		//end pos
		double ex = e.position.x;
		double ey = e.position.y;
		
		CGPathMoveToPoint(pathToDraw, nil, sx, sy);
		CGPathAddLineToPoint(pathToDraw, nil, ex, ey);
		
		retVal.strokeColor = DEFAULT_EDGE_COLOR;
		retVal.lineWidth = CONNECTION_SCALAR * [board EDGE_THICKNESS];
		retVal.name = nil;
		[retVal setZPosition:2];
		retVal.path = pathToDraw;
		connection = retVal;
		connection.zPosition = 9;
		
		double dx = ex - sx;
		double dy = -(ey - sy);
		double dir = atan2(dy, dx) / (2.0 * M_PI);
		dir = dir + 1.0;
		dir *= 360.0;
		dir = (double)(((int)dir) % 360);
		
		double d_angles[8] = { 135.0, 90.0, 45.0, 180.0, 0.0, 225.0, 270.0, 315.0 };
		
		for(direction d = NW; d <= SE; d++){
			if(fabs(dir - d_angles[d]) <= (45.0 / 2.0)){
				touchVector = d;
			}
		}
		
		double lengthSQ = sqrt(dx*dx + dy*dy);
		double m = self.size.width / 10.0;
		
		if(lengthSQ >= m){
			[self addChild:e];
			[self addChild:connection];
		}else{
			e = NULL;
			connection = NULL;
		}

		break;
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
	if(touchDown){
		commitTouch = true;
		touchDown = false;
		
		bool selectFlag = (e == NULL && connection == NULL);
		
		assert(s);
		
		CGPoint selPt = s.position;
		
		if(s != NULL){
			[s removeFromParent];
			s = NULL;
		}
		if(e != NULL){
			[e removeFromParent];
			e = NULL;
		}
		if(connection != NULL){
			[connection removeFromParent];
			connection = NULL;
		}
		
		//this will happen if distance too small and node should be selected
		if(selectFlag){
			NSArray *nodes = [self nodesAtPoint:selPt];
			for(SKNode* node in nodes){
				for(player* p in _players){
					if ([p selectPiece:node]){
						break;
					}
				}
			}
		}else{	//this will happen if length is long and we should move with input
			player* p = _players[_currentPlayerMove];
			//if this player successfully completes turn then move to next player
			if ([p performMove:touchVector withPieces: _players]){
				_currentPlayerMove = (_currentPlayerMove + 1) % _players.count;
				
				while([_players[_currentPlayerMove] hasMovablePieces:_players] == false){
					_currentPlayerMove = (_currentPlayerMove + 1) % _players.count;
				}
			}
		}
	}
	
	bool hasMoves[4] = { false, false, false, false };
	int numPlayersWithMove = 0;
	int winningPlayer = 0;
	
	for(locationState i = P1; i <= _num_players; i++){
		hasMoves[i - 1] = [[_players objectAtIndex:(i-1)] hasMovablePieces:_players];
		if(hasMoves[i - 1]){
			numPlayersWithMove++;
		}
	}
	
	if(numPlayersWithMove == 1){
		for(locationState i = P1; i <= _num_players; i++){
			if(hasMoves[i - 1]){
				winningPlayer = i;
				break;
			}
		}
	}
	
	if(winningPlayer != 0){
		//transition for victory screen here
		GameOverScene* s = [[GameOverScene alloc] initWithSize:self.size];
		s.winner = winningPlayer;
		[s setBackgroundColor:[SKColor blackColor]];
		
		SKTransition *transition = [SKTransition flipVerticalWithDuration:1.0];
        [self.view presentScene:s transition:transition];
	}
}

-(void)update:(CFTimeInterval)currentTime {
	if(_updateReady == false){
		return;
	}

	for(int i = 0; i < _players.count; i++){
		if(i != _currentPlayerMove){
			[_players[i] unselect];
		}else{
			[_players[i] select];
		}
	}
	
	if(_playerUp != NULL){
		[_playerUp removeFromParent];
		_playerUp = NULL;
	}
	
	_playerUp = [SKShapeNode shapeNodeWithCircleOfRadius:(self.size.width / 6.0)];
	_playerUp.fillColor = PLAYERCOLOR(_currentPlayerMove + 1);
	_playerUp.strokeColor = [SKColor blackColor];
	_playerUp.position = CGPointMake(self.size.width, 0);
	[self addChild:_playerUp];
	
	if(_playerUpLabel != NULL){
		[_playerUpLabel removeFromParent];
		_playerUpLabel = NULL;
	}
	
	
	SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:PREFERRED_FONT];
	label.fontSize = 50;
	label.fontColor = [SKColor blackColor];
	label.text = [NSString stringWithFormat:@"%d", _currentPlayerMove + 1];
	label.position = CGPointMake(self.size.width-(self.size.width / 16.0), (self.size.width / 128.0));
	
	_playerUpLabel = label;
	
	[self addChild:_playerUpLabel];
	
}

@end
