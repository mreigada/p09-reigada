//
//  GameOverScene.m
//  p09-reigada
//
//  Created by Matthew Reigada on 5/15/16.
//  Copyright © 2016 Matthew Reigada. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameOverScene.h"
#import "menuScene.h"

@implementation GameOverScene

	-(void)didMoveToView:(SKView *)view {
	
	
	    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:PREFERRED_FONT];
    	label.fontSize = 50;
		label.fontColor = PLAYERCOLOR(_winner);
    	label.text = [NSString stringWithFormat:@"Player %d Wins.", _winner];
		label.position = CGPointMake(self.size.width / 2, self.size.height / 2);
		
		[self addChild:label];
		
	}

	-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
		for(UITouch* touch in touches){
			menuScene* s = [[menuScene alloc] initWithSize:self.size];
		
			SKTransition *transition = [SKTransition flipVerticalWithDuration:1.0];
			[self.view presentScene:s transition:transition];
		}
	}

@end